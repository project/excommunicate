<?php

/**
 * @file
 * File to implement drupal_mail_wrapper(). This can't be in the module file
 * as that would prevent any other modules from implementing
 * drupal_mail_wrapper().
 */

/**
 * Implementation of drupal_mail_wrapper().
 */
function drupal_mail_wrapper($message) {
  return excommunicate_mail_wrapper($message);
}

