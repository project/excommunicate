Excommunicate
=============

This module will prevent all emails from being sent to users who's accounts are
blocked. In essence, these users are "excommunicated" by the site.

Overview
========

Due to the way Drupal's mail system works, we don't know the user accounts
associated with email addresses for the message being sent. In fact, the email
addresses may not even been associated with a user account. To get around this,
we parse the email addresses out of the To, CC, and BCC headers, and provide a
hook for modules to link those addresses to a Drupal user account.

Installation
============

Before installing this module, make sure PHP has the "IMAP" extension
installed. While this module doesn't use the IMAP protocol, it does use the
email header parsing functions contained within this extension. You can check
to see if the IMAP extension is installed by checking phpinfo on your web
server using the devel module, or by running "php -m" from the command line.

This module also implements a custom SMTP library to handle filtering of
messages. Only one SMTP library can be installed at a time within a given
Drupal site. If you wish to use another module that provides an SMTP library,
it must support Excommunicate for this module to function. During installation,
if another SMTP library is enabled, it will be left as-is and Excommunicate
will display a message with the path to the other library.

After ensuring that the IMAP extension is installed, place this module in
sites/all/modules, and enable it. There's no configuration for the module at
the moment. By default, and messages with an address matching a user's email
address will have that address removed from the TO, CC, or BCC email headers.
As PHP requires at least one "TO" address, if that is empty after filtering,
no message will be sent at all. If there are additional email addresses
associated with a user that should be checked, hooks are provided to allow
Excommunicate to be aware of them. See excommunicate.api.php for details.

