<?php

/**
 * @file
 * Hooks provided by the Excommunicate module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Return a user account associated with an email address if it exists.
 *
 * Use this hook to map additional email addresses to a Drupal user account.
 * Note that the results of this hook are cached per request.
 *
 * @param $address
 *   The email address to search for.
 *
 * @return
 *   The user account associated with the email address if it exists.
 */
function hook_excommunicate_mail_account($address) {
  if ($account = user_load(array('mail' => $address))) {
    return $account;
  }
}

/**
 * Return an array of email addresses associated with a Drupal user account.
 *
 * This hook is for modules to associate alternate email addresses with an
 * account, such as those stored in other tables or fields on a user object.
 *
 * @param $account
 *   The user object to return email addresses for.
 *
 * @return
 *   An array of email addresses.
 */
function hook_excommunicate_user_addresses($account) {
  return array($account->mail);
}

/**
 * @} End of "addtogroup hooks".
 */

